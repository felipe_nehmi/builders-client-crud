package com.builders.controller;

import com.builders.model.Client;

import java.util.List;

public class PaginatedResponse {
    public List list;
    public Integer page;
    public Integer size;
    public Integer totalResults;
    public Integer pageCount;

    public PaginatedResponse(List list, Integer page, Integer size, Integer totalResults, Integer pageCount) {
        this.list = list;
        this.page = page;
        this.size = size;
        this.totalResults = totalResults;
        this.pageCount = pageCount;
    }

    public List getList() {
        return list;
    }

    public void setList(List list) {
        this.list = list;
    }

    public Integer getPage() {
        return page;
    }

    public void setPage(Integer page) {
        this.page = page;
    }

    public Integer getSize() {
        return size;
    }

    public void setSize(Integer size) {
        this.size = size;
    }

    public Integer getTotalResults() {
        return totalResults;
    }

    public void setTotalResults(Integer totalResults) {
        this.totalResults = totalResults;
    }

    public Integer getPageCount() {
        return pageCount;
    }

    public void setPageCount(Integer pages) {
        this.pageCount = pages;
    }
}
