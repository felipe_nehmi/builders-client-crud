package com.builders.controller;

import java.sql.Timestamp;

public class ClientResource {
    String name;
    String cpf;
    Timestamp birthDate;

    public String getName() {
        return name;
    }

    public String getCpf() {
        return cpf;
    }

    public Timestamp getBirthDate() {
        return birthDate;
    }
}
