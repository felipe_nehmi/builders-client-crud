package com.builders.controller;

import com.builders.dao.ClientDao;
import com.builders.model.Client;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/client")
public class ClientController {

    @Autowired
    ClientDao clientDao;

    @GetMapping
    public PaginatedResponse searchClients(@RequestParam("searchString") String searchString,
                                           @RequestParam("page") Integer page,
                                           @RequestParam("size") Integer size) {
        return clientDao.searchPaginated(searchString, page, size);
    }

    @PostMapping
    public Long insertClient(@RequestBody ClientResource clientResource) {
        return clientDao.insert(clientResource);
    }

    @PutMapping("/{id}")
    public ResponseEntity updateClient(@PathVariable Long id, @RequestBody ClientResource clientResource) {
        Client updated = clientDao.update(id, clientResource);
        if (updated == null) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok().build();
    }

    @PatchMapping("/{id}")
    public ResponseEntity patchClient(@PathVariable Long id, @RequestBody ClientResource clientData){
        Client updated = clientDao.patch(id, clientData);
        if (updated == null) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok().build();
    }

    @DeleteMapping("/{id}")
    public ResponseEntity deleteClient(@PathVariable Long id) {
        Boolean deleted = clientDao.delete(id);
        if (!deleted) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok().build();
    }

}
