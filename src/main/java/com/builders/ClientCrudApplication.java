package com.builders;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication(scanBasePackages = {"com.builders"})
public class ClientCrudApplication {

	public static void main(String[] args) {
		SpringApplication.run(ClientCrudApplication.class, args);
	}

}
