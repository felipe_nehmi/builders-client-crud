package com.builders.dao;

import com.builders.controller.ClientResource;
import com.builders.controller.PaginatedResponse;
import com.builders.model.Client;
import com.builders.repository.ClientRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;

@Service
public class ClientDao {
   @Autowired
   private ClientRepository clientRepository;

   @PersistenceContext
   private EntityManager em;

   @Transactional
   public Long insert(ClientResource clientResource) {
      Client clientModel = new Client(clientResource.getName(),
              clientResource.getCpf(),
              clientResource.getBirthDate());
      return clientRepository.save(clientModel).getId();
   }

   @Transactional
   public Client update(Long id, ClientResource clientResource){
      return clientRepository.findById(id).map(row -> {
         row.setName(clientResource.getName());
         row.setCpf(clientResource.getCpf());
         row.setBirthDate(clientResource.getBirthDate());
         return clientRepository.save(row);
      }).orElse(null);
   }

   @Transactional
   public Client patch(Long id, ClientResource clientResource) {
      return clientRepository.findById(id).map(row -> {
         if (clientResource.getName() != null) {
            row.setName(clientResource.getName());
         }
         if (clientResource.getCpf() != null) {
            row.setCpf(clientResource.getCpf());
         }
         if (clientResource.getBirthDate() != null) {
            row.setBirthDate(clientResource.getBirthDate());
         }
         return clientRepository.save(row);
      }).orElse(null);
   }

   @Transactional
    public Boolean delete(Long id) {
      return clientRepository.findById(id).map(row -> {
         clientRepository.deleteById(id);
         return true;
      }).orElse(false);
    }

   public PaginatedResponse searchPaginated(String searchString, Integer page, Integer size){
      Integer offset = (page - 1) * size;
      Query baseQuery = em.createQuery("SELECT c FROM Client c" +
              " WHERE c.name LIKE :searchString OR c.cpf LIKE :searchString" +
              " ORDER BY c.id ASC")
              .setParameter("searchString", "%" + searchString + "%");

      int total = baseQuery.getResultList().size();
      int pageCount = (int) Math.ceil((double) total / size);

      List resultList = baseQuery
              .setFirstResult(offset)
              .setMaxResults(size)
              .getResultList();

      return new PaginatedResponse(
              resultList, page, size, total, pageCount);
   }
}
